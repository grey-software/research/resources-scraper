from opengraph_parse import parse_page
import xml.etree.ElementTree as ET


def getSiteUrls():
    tree = ET.parse("./resources-sitemap.xml")
    root = tree.getroot()
    urlElements = root.findall(".//url//loc")
    return [urlElement.text for urlElement in urlElements]


def fetchResourceData(resourceLink):
    # Grab open graph tags of current link
    ogTags = parse_page(
        resourceLink, ["og:title", "og:description", "og:image"])
    
    try:
        resource = {
            "title": ogTags["og:title"],
            "link": resourceLink,
            "image": ogTags["og:image"],
            "description": ogTags["og:description"]
        }
    except:
        resource = {
            "link": resourceLink,
        }
    finally:
        return resource
