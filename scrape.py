import scrapy
import json
from utils import fetchResourceData, getSiteUrls

class WebScraping(scrapy.Spider):
    name = "resources-scraper"
    start_urls = getSiteUrls()[10:12]

    def parse(self, response):
        # extracting page tile
        page_title = response.css('h1::text').get().replace('\n', '').strip()

        # grabbing nuxt content class
        pageContent = response.css('div.nuxt-content')

        # create list of current page's links
        pageLinks = set(pageContent.css("a::attr(href)").getall())

        resources = []
        
        for link in pageLinks:
            if link.startswith("http"):
                resources.append(fetchResourceData(link))
  
        jsonToWrite = {"title": page_title, "resources": resources}

        with open('data.json', 'w') as f:
            json.dump(jsonToWrite, f, indent=4)